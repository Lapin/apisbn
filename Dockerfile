FROM python:3.5-alpine

RUN apk add --update --no-cache g++ gcc libxslt-dev libxml2-dev \
    && rm -rf /var/cache/apk/*

RUN pip install isbntools flask requests lxml
RUN apk del g++ gcc

ENV SERVICE_NAME "apisbn"

WORKDIR /apisbn

ADD apisbn /apisbn

EXPOSE 5000

CMD ["python", "apisbn.py"]

