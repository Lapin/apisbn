#isbnlib
from isbnlib import is_isbn10, is_isbn13, to_isbn13
from isbnlib import meta #, desc, cover, isbn_from_words

from isbnlib import NotValidISBNError #, ISBNLibException
from isbnlib.dev import DataNotFoundAtServiceError as DataNotFound
from isbnlib.dev import NoDataForSelectorError as NoData
from isbnlib.dev import DataWrongShapeError as DataWrong

#bnf
from bnf import Bnf

#notes:
#    isbn: 9782373740189 raise DataWrongShapeError (search_on_isbnlib)
#    isbn:

class Document:
    """
        Class that represent ISBN record. Source used are : bnf.fr, openlibrary.org, worldcat.org and googlebook.
    """
    def __init__(self, isbn, **kwargs):
        """
        :param isbn: valid isbn number (10 or 13) (required)
        :param title: str
        :param author: str
        :param authors_mentions: list, list of mention of the authors
        :param language: str, eg. FR, UK, DE, ..
        :param collection: str
        :param volume: str
        :param edition: str
        :param source: str, source of the api that provide information
        :param year: str, edition year
        :param price: str, possible price
        """
        if is_isbn10(isbn):
            self.isbn = to_isbn13(isbn)
        elif is_isbn13(isbn):
            self.isbn = isbn
        else:
            raise NotValidISBNError(isbn)
        self.title = kwargs.pop('title', None)
        self.author = kwargs.pop('author', None)
        self.authors_mentions = kwargs.pop('authors_mention', [])
        self.language = kwargs.pop('language', None)
        self.description = kwargs.pop('description', None)
        self.collection = kwargs.pop('collection', None)
        self.volume = kwargs.pop('volume', None)
        self.edition = kwargs.pop('edition', None)
        self.year = kwargs.pop('year', None)
        self.price = kwargs.pop('price', None)
        self.source = kwargs.pop('source', None)
        self.as_record = None
        self.authors_mention = None

    def _search_on_isbnlib(self, service):
        """
        search with meta on isbnlib on a particular service
        return json dict or None
        """
        if service not in ['wcat', 'openl', 'goob']:
            raise ValueError('please read isbnlib documentation')
        try:
            return meta(self.isbn, service=service)
        except (DataNotFound, NoData, DataWrong):
            return None

    def _parse_from_isbnlib(self, d_meta):
        """
        parse returned dictionnary from the d_meta isbnlib method
        and set Document attributes
        """
        self.title = d_meta.get('Title')
        self.authors_mention = d_meta.get('Authors')
        if self.authors_mention:
            self.author = self.authors_mention[0]
        self.language = d_meta.get('Language')[:2].upper()
        self.edition = d_meta.get('Publisher')
        self.year = d_meta.get('Year')
        return self

    def search_on_wcat(self):
        """
        search document on worldcat.org
        return None if not find, self with filled attributes otherwise
        """
        find = self._search_on_isbnlib('wcat')
        if find:
            self.source = 'worldcat.org'
            return self._parse_from_isbnlib(find)
        return None

    def search_on_openl(self):
        """
        search document on openlibrary.org
        return None if not find, self with filled attributes otherwise
        """
        find = self._search_on_isbnlib('openl')
        if find:
            self.source = 'openlibrary.org'
            return self._parse_from_isbnlib(find)
        return None

    def search_on_google(self):
        """
        search document on google book
        return None if not find, self with filled attributes otherwise
        """
        find = self._search_on_isbnlib('goob')
        if find:
            self.source = 'google book services'
            return self._parse_from_isbnlib(find)
        return None

    def search_on_isbnlib(self):
        """
        will in order on:
            openlibrary.org
            worldcat.org
            google book
        """
        find = self.search_on_openl()
        if not find:
            find = self.search_on_wcat()
        if not find:
            find = self.search_on_google()
        return find

    def search_on_bnf(self):
        """
        search on bnf.fr with the Bnf lib
        """
        bnf = Bnf()
        find = bnf.get_record(self.isbn)
        if find:
            self.source = 'catalogue.bnf.fr'
            for k, val in find.to_dict().items():
                setattr(self, k, val)
            self.as_record = find
            return find
        return None


    def search(self, source=None):
        """
        search in order on :
            - catalogue.bnf.fr (bnf)
            - openlibrary.org (openl)
            - worldcat.org (wcat)
            - google book (google)
        you can specify source if you want just one
        """
        if not source:
            find = self.search_on_bnf()
            if find:
                return self
            return self.search_on_isbnlib()
        elif source == 'bnf':
            return self.search_on_bnf()
        elif source == 'openl':
            return self.search_on_openl()
        elif source == 'wcat':
            return self.search_on_wcat()
        elif source == 'google':
            return self.search_on_google()
        raise ValueError('source not valid')

    def to_dict(self):
        """
        return Document as a dict, helper to serialize
        """
        return {k: v for k, v in vars(self).items() if v is not None and k != 'as_record'}

    def __str__(self):
        return str(vars(self))
