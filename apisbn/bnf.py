"""
module to talk with BNF api.
ref:
    -- http://catalogue.bnf.fr
    -- http://www.bnf.fr/documents/service_sru_bnf.pdf

for unimarcxchange specification:
    --
    http://www.bnf.fr/fr/professionnels/anx_formats/a.unimarc_manuel_format_bibliographique.html#SHDC__Attribute_BlocArticle10BnF
"""

from lxml import etree
import requests

class Bnf:
    """
    class used to interact with bnf api
    """
    def __init__(self):
        self._base_url = 'http://catalogue.bnf.fr/api/SRU'
        self._base_args = '?version=1.2&operation=searchRetrieve&query='
        self._query = '{}%20{}%20%22{}%22' # bib.X, operand, search
        self._base_end_args = '&recordSchema=unimarcxchange&maximumRecords={}&startRecord=1'
        # schema, MaxRecords

    @property
    def _base(self):
        " forge base url query "
        return self._base_url + self._base_args

    def query(self, bib, operand, search, maxrec=1):
        """
        forge query for bnf
        """
        return self._base + self._query.format(bib, operand, search) + self._base_end_args.format(maxrec)

    @staticmethod
    def _request(req):
        """
        simple get with requests and try to parse content and return xml 'records' elem
        """
        resp = requests.get(req)
        if resp:
            return resp
        return None

    @staticmethod
    def _elem_find(elem, find, _all=False):
        """
        helper to find in xml elem
        """
        elem_basename = '{http://www.loc.gov/zing/srw/}'
        if _all:
            return elem.findall(elem_basename+find)
        return elem.find(elem_basename+find)

    def _request_record(self, req):
        """
        Request bnf api
        return xml elem of wanted data if there is only one record
        return None otherwise
        """
        resp = self._request(req)
        if not resp:
            return None
        xml = etree.fromstring(resp.content)
        if self._elem_find(xml, 'numberOfRecords').text == '0':
            return None
        return self._elem_find(xml, 'records')

    def get_unimarc_record_from_isbn(self, isbn):
        """
        return response from bnf with unimarcXchanche format
        """
        records = self._request_record(self.query('bib.isbn', 'adj', isbn, maxrec='1'))
        if records is not None:
            record = self._elem_find(records, 'record')
            data = self._elem_find(record, 'recordData')
            return data.getchildren()[0]
        return None

    def get_collection_records(self, search):
        """ use with a real collection name return by bnf api on one document """
        results = list()
        records = self._request_record(self.query('bib.everywhere', 'all', search, maxrec='100')) or []
        for record in records:
            data = self._elem_find(record, 'recordData')
            results.append(data.getchildren()[0])
        return results

    def get_record(self, isbn):
        """
        return Record if exist or None
        """
        rec = self.get_unimarc_record_from_isbn(isbn)
        if rec is not None:
            return Record(rec)
        return None

    def get_collection(self, search):
        coll = self.get_collection_records(search)
        return [Record(rec) for rec in coll]

class Record:
    """
    Use to parse unimarc xml and represent Bnf Record
    """
    def __init__(self, record):
        self.attr_to_clean = ['attr_to_clean', 'record', 'unibase', 'fields', 'unimarc_map']
        self.unimarc_map = {'title': {'ref': [('200', 'a')], 'func': "value.upper()"},
                            'language': {'ref': [('101', 'a')], 'func': "value[:2].upper()"},
                            'isbn': {'ref': [('010', 'a'), ('073', 'a')], 'func': "value.replace('-', '')"},
                            'authors_mention': {'ref': [('200', 'f')], 'func': "self._check_authors_mention(value)"},
                            'author': {'ref':[('700', 'b')], 'func': "self._check_author(value)"},
                            'author_date': {'ref':[('700', 'f')]},
                            'editions': {'ref': [('210', 'c')]},
                            'collection': {'ref': [('225', 'a')], 'func': "self._check_collection(value)"},
                            'volume': {'ref': [('225', 'v')], 'func': "self._check_collection(value)"},
                            'description': {'ref': [('330', 'a')]},
                            'price': {'ref': [('010', 'd')]}
                           }

        self.record = record
        self.unibase = '{info:lc/xmlns/marcxchange-v2}'
        self.fields = [Field(f) for f in self.record.findall(self.unibase+'datafield')]
        self.get_common_data()

    def get_fields(self, tag):
        " return all fields that match provided tag "
        return [f for f in self.fields if f.tag == tag]

    def get_field(self, tag):
        " return the first field that match provived tag or None"
        try:
            return self.get_fields(tag)[0]
        except IndexError:
            return None

    def get_value(self, tag, code):
        """ return value at the provide tag and code if exist """
        field = self.get_field(tag)
        if field:
            return field.get_code(code)
        return None

    def _set_attr(self, attr, tag, code):
        """
        helper to set attribute from tag and code
        """
        value = self.get_value(tag, code)
        setattr(self, attr, value)
        return value

    def get_common_data(self):
        """
        used to set all identified data
        retreive data, lint them and set attributes
        """
        for attr, data in self.unimarc_map.items():
            func = data.get('func')
            for ref in data.get('ref', []):
                value = self._set_attr(attr, ref[0], ref[1])
                if value:
                    break
            if func and value:
                setattr(self, attr, eval(func))

    def _check_authors_mention(self, value):
        " sanitize authors_mention value "
        if value:
            sec = self.get_value('200', 'g')
            if sec:
                return [value, sec]
            return [value]
        return None

    def _check_author(self, value):
        " sanitize authors value "
        authors = self.get_value('700', 'a')
        if value and authors:
            return value + ' ' + authors
        elif value:
            return value
        elif authors:
            return authors
        return None

    def _check_collection(self, value):
        " sanitize collection value "
        if self.get_field('225') and self.get_field('225').ind1 != '1' or not self.get_field('225'):
            return None
        return value

    def to_dict(self):
        " return Record object as dict "
        return {k: v for k, v in vars(self).items() if k not in self.attr_to_clean and v is not None}

    def pprint(self):
        " pretty print use mostly for debug "
        template = '-- {:20}:{}'
        try:
            print(self.title.upper())
        except AttributeError:
            print('ERROR TITLE')
        for k, val in self.to_dict().items():
            print(template.format(k, val))

    def dprint(self):
        " debug print, more verbose "
        template = '-- {:20}:{}'
        for field in self.fields:
            print(field)
            for k, val in field.code.items():
                print(template.format(k, val))

    def debug_dict(self):
        " as the name says .. "
        res = self.to_dict()
        res.update({'fields': {f.tag: {'ind1':f.ind1,
                                       'ind2':f.ind2,
                                       'code':f.code} for f in self.fields}})
        return res

class Field:
    """
    class that parse and represent field in Record
    """
    def __init__(self, field):
        self.field = field
        self._parse_field()
        self._parse_code()

    def _parse_field(self):
        " set tag, ind1, ind2 attr"
        for k, val in self.field.items():
            setattr(self, k, val)

    def _parse_code(self):
        " set code attr as dict "
        self.code = dict()
        for raw_code in self.field.getchildren():
            code = raw_code.items()[0][1]
            self.code.update({code: raw_code.text})

    def get_code(self, code):
        " if code value if exist or None "
        try:
            return self.code[code]
        except KeyError:
            return None

    def __str__(self):
        if hasattr(self, 'tag') and hasattr(self, 'ind1') and hasattr(self, 'ind2'):
            return 'field: tag = {tag}, ind1 = {ind1}, ind2 = {ind2}'.format(**vars(self))
        return super().__str__()
