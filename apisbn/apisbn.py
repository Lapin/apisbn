#coding utf-8
"""
little front end with flask
"""
from functools import wraps

## flask
from flask import Flask, jsonify #, abort

from document import Document, NotValidISBNError

def as_doc(func):
    "decorator that passed Document(isbn) to the function"
    @wraps(func)
    def inner(*args, **kwargs):
        try:
            doc = Document(kwargs.get('isbn', ''))
        except NotValidISBNError:
            return jsonify({'message': 'Not valid ISBN number'}), 400
        return func(doc)
    return inner

app = Flask(__name__)

#@app.errorhandler(404)
def error404():
    return jsonify({'message': 'Resource not find'}), 404

@app.route('/v1/isbn/<isbn>')
@as_doc
def get_isbn(isbn):
    """
    search information on isbn with all sources.
    in order:
        - bnf.fr
        - openlibrary.org
        - worldcat.org
        - google book
    :param isbn: str of a valid ISBN number
    """
    find = isbn.search()
    if find:
        return jsonify(isbn.to_dict()), 200
    return error404()

@app.route('/v1/search/bnf/<isbn>')
@as_doc
def searchbnf(isbn):
    """
    search only in bnf.fr
    """
    find = isbn.search_on_bnf()
    if find:
        return jsonify(isbn.to_dict()), 200
    return error404()

@app.route('/v1/search/bnf/debug/<isbn>')
@as_doc
def searchdebugbnf(isbn):
    """
    search only on bnf.fr
    return all data (even all not normalize) from the bnf response
    """
    find = isbn.search_on_bnf()
    if find:
        return jsonify(isbn.as_record.debug_dict()), 200
    return error404()

@app.route('/v1/search/openl/<isbn>')
@as_doc
def searchopenl(isbn):
    """
    search only in openlibrary.org
    """
    find = isbn.search_on_openl()
    if find:
        return jsonify(isbn.to_dic()), 200
    return error404()

@app.route('/v1/search/wcat/<isbn>')
@as_doc
def searchwcat(isbn):
    """
    search only in worldcat.org
    """
    find = isbn.search_on_wcat()
    if find:
        return jsonify(isbn.to_dict()), 200
    return error404()

@app.route('/v1/search/google/<isbn>')
@as_doc
def searchgoob(isbn):
    """
    search only in google books
    """
    find = isbn.search_on_google()
    if find:
        return jsonify(isbn.to_dict()), 200
    return error404()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
