# APISBN

This api is based on isbnlib.

requirement:

    * python3.5
    * flask_restful

you can also simply build the docker and run:

    * docker build -tag apisbn .
    * docker run --name apisbn -p 5000:5000 -d apisbn

# ENDPOINT

* /v1/isbn/<isbn>:    return meta data for the given isbn
* eg: /isbn/978-2-7493-0701-5
* eg: /isbn/9782290345894

* /v1/bnf/<isbn>
* /v1/bnf/debug/<isbn>
* /v1/openl/<isbn>
* /v1/wcat/<isbn>
* /v1/google/<isbn>
